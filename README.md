# PG&E Data Scraper

Opens the PG&E account login via a PhantomJS instance, navigates to the
appropriate page containing usage chart data, then makes AJAX requests for
_all_ electricity usage data that can be returned.

## CLI Usage
| Argument | Shorthand | Description |
| --- | --- | --- |
| creds-file | r | A credentials file to use for authentication. Must be a JSON file with "username" and "password" keys. Can be used instead of the --username and --password options. |
| to-file    | t | The file path to save the resulting usage data to. Data is saved in JSON format. |
| username   | u | Username to user for authentication. Must be used with the --password option. |
| password   | p | Password to user for authentication. Must be used with the --username option. |
| help       | h | Print this usage guide. |
