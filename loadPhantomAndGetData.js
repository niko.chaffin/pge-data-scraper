const phantomjs = require('phantom');
const moment = require('moment');
const Promise = require('bluebird'); // jshint ignore:line
const promiseWhile = require('while-promise')(Promise);

const qTimeout = require('./utils/qTimeout');
const logUrlChange = require('./utils/logUrlChange');
const waitForElements = require('./utils/waitForElements');
const getPGEAccountDataUrl = require('./utils/getPGEAccountDataUrl');
const getAccountUuidFromPage = require('./utils/getAccountUuidFromPage');

var startUrl = 'https://pge.com';
var phantomSettings = [
    '--load-images=false',
    '--local-to-remote-url-access=true',
    '--web-security=false',
];

/**
 * Navigate to login page of PG&E.
 *
 * @param {Page} page
 * @return {Promise}
 */
function navigateToLoginPage(page) {
    return page.open(startUrl);
}

/**
 * Submit the login form on PG&E's site.
 *
 * @param {Page} page
 * @param {String} username
 * @param {String} password
 * @return {Promise}
 */
function submitLoginForm(page, username, password) {
    return waitForElements(page, '.login-form', '#username', '#password', '#home_login_submit')
    .then(() => {
        return page.evaluate(function(username, password) {
            $('.login-form').find('#username').val(username);
            $('.login-form').find('#password').val(password);
        }, username, password);
    }).then(() => {
        console.log("> Filled out login form.");
        return page.evaluate(function() {
          $('.login-form').find('#home_login_submit').click();
        });
    }).then(() => {
        console.log("> Submitted login form.");
    });
}

/**
 * Click the "My Usage" link on the PG&E dashboard.
 *
 * @param {Page} page
 * @return {Promise}
 */
function clickMyUsageLink(page) {
    return waitForElements(page, '#myusage')
    .then(() => {
        return page.evaluate(function() {
            $('#myusage').click();
        });
    });
}

/**
 * Switch the displayed energy type to combined, electricity, or gas.
 *
 * @param {Page} page
 * @param {String} fuel
 * @return {Promise}
 */
function switchEnergyChartToFuelType(page, fuel) {
    let fuelTypes = ["combined", "electricity", "gas"];
    let index = fuelTypes.indexOf(fuel);
    if (index === -1) {
        index = 0;
    }
    return waitForElements(page, '.fuel-type-selector')
    .then(() => {
        return page.evaluate(function(index) {
            var scope = angular.element(document.querySelector('.fuel-type-selector')).scope();
            scope.ctrl.selectOption(scope.accountOptions[index]); // select "ELECTRICITY" option
        }, index);
    });
}

/**
 * Alias of three methods to navigate to energy chart page
 *
 * @param {Page} page
 */
function navigateToEnergyChartPage(page, username, password) {
    return navigateToLoginPage(page)
    .then(function() {
        return submitLoginForm(page, username, password);
    }).then(() => {
        return clickMyUsageLink(page);
    });
}

/**
 * Sends an AJAX request through the PhantomJS page, then resolves the returned
 * promise once the response arrives.
 *
 * @param {Page} page
 * @param {String} accountUuid
 * @param {String} startDate formated as YYYY-MM-DD
 * @param {String} endDate formated as YYYY-MM-DD
 * @return {Promise<Array>} Resolves into an array of meter data
 */
function requestEnergyChartData(page, accountUuid, startDate, endDate) {
    return page.evaluate(function(accountUuid, startDate, endDate, getPGEAccountDataUrl) {
        window._meterReads = undefined;
        var params = {
            startDate: startDate,
            endDate: endDate,
            aggregateType: 'hour',
        };
        $.ajax({
            method: 'get',
            url: getPGEAccountDataUrl(accountUuid, params),
            dataType: 'application/json',
            complete: function(response) {
                var meterReads = JSON.parse(response.responseText).reads;
                window._meterReads = meterReads;
            }
        });
    }, accountUuid, startDate, endDate, getPGEAccountDataUrl)
    .then(() => {
        let meterReads = null;
        return promiseWhile(() => {
            return meterReads === null;
        }, () => {
            return qTimeout(100)
            .then(() => {
                return page.evaluate(function() {
                    var _meterReads = window._meterReads;
                    window._meterReads = undefined;
                    return _meterReads;
                });
            }).then((_meterReads) => {
                meterReads = _meterReads;
            });
        }).then(() => meterReads);
    });
}

/**
 * Helper function to confirm the sorting of meter reads
 *
 * @param {String} a.startTime A valid datetime string
 * @param {String} b.startTime A valid datetime string
 * @return {Number}
 */
function sortMeterReadByStartTime(a, b) {
    a = moment(a.startTime);
    b = moment(b.startTime);
    if (a < b) {
        return -1;
    } else if (a > b) {
        return 1;
    } else {
        return 0;
    }
}

/**
 * Get all the electricity data for the PG&E account, requesting one month at
 * a time and moving forwards in time.
 *
 * @param {Page} page
 * @param {String} accountUuid
 * @return {Promise<Array>} Resolves into an array of meter data
 */
function getElectricityData(page, accountUuid) {
    console.log("> Getting electrical data...");
    let nextMonth = () => moment().add(1, 'months');
    // Set the month start and end for next month, since we're going to be
    // subtracting one month each time
    let [monthStart, monthEnd] = [nextMonth().startOf('month'), nextMonth().endOf('month')];
    let lastReadSet, meterReads = [];
    let rangeEnd = nextMonth().endOf('month');
    return promiseWhile(() => {
        return typeof lastReadSet === 'undefined' || lastReadSet.length > 0;
    }, () => {
        [monthStart, monthEnd] = [monthStart, monthEnd].map(m => m.subtract(1, 'months'));
        let [startDate, endDate] = [monthStart, monthEnd].map(m => m.format('YYYY-MM-DD'));
        return requestEnergyChartData(page, accountUuid, startDate, endDate)
        .then((_meterReads) => {
            // Sort the reads from oldest to newest
            _meterReads.sort(sortMeterReadByStartTime).reverse();
            if (_meterReads.length > 0) {
                let firstDate = moment(_meterReads[0].startTime);
                let lastDate = moment(_meterReads[_meterReads.length - 1].startTime);
                console.log(`> Collected ${_meterReads.length} readings for ${firstDate.format('YYYY-MM-DD')} through ${lastDate.format('YYYY-MM-DD')}`);
            }
            lastReadSet = _meterReads;
            meterReads = meterReads.concat(_meterReads);
        });
    }).then(() => {
        let rangeStart = monthStart.add(1, 'months');
        meterReads.reverse(); // Sort the reads from oldest to newest
        return {
            createdAt: moment().format('YYYY-MM-DD'),
            electricity: {
                rangeStart: rangeStart.format('YYYY-MM-DD'),
                rangeEnd: rangeEnd.format('YYYY-MM-DD'),
                reads: meterReads,
            }
        };
    });
}

// DO IT
module.exports = function loadPhantomAndGetData(username, password) {
    let phantom;
    return phantomjs.create(phantomSettings).then((_phantom) => {
        phantom = _phantom;
        return phantom.createPage();
    }).then((page) => {
        page.on('onError', () => {}); // mute errors
        page.on('onUrlChanged', () => logUrlChange(page));
        // page.on('onConsoleMessage', (message) => console.log("[page]: " + message));

        return navigateToEnergyChartPage(page, username, password)
        .then(() => {
            return switchEnergyChartToFuelType(page, "electricity");
        }).then(() => {
            return getAccountUuidFromPage(page);
        }).then((accountUuid) => {
            return getElectricityData(page, accountUuid);
        }).then((meterReads) => {
            phantom.kill();
            phantom.exit(0);
            return meterReads;
        }).catch((error) => {
            console.error(error);
            phantom.kill();
            return phantom.exit(1);
        });
    });
};
