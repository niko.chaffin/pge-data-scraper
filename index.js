const fs = require('fs');
const path = require('path');
const commandLineArgs = require('command-line-args');
const commandLineUsage = require('command-line-usage');

const loadPhantomAndGetData = require('./loadPhantomAndGetData');
const optionDefinitions = [
    {
        name: 'creds-file',
        alias: 'r',
        description: 'A credentials file to use for authentication. Must be a JSON file with [bold]{username} and [bold]{password} properties. Can be used instead of the [bold]{--username} and [bold]{--password} options.',
        type: String
    },
    {
        name: 'to-file',
        alias: 't',
        description: 'The file path to save the resulting usage data to. Data is saved in JSON format.',
        type: String,
    },
    {
        name: 'username',
        alias: 'u',
        description: 'Username to user for authentication. Must be used with the [bold]{--password} option.',
        type: String,
    },
    {
        name: 'password',
        alias: 'p',
        description: 'Password to user for authentication. Must be used with the [bold]{--username} option.',
        type: String,
    },
    {
        name: 'help',
        alias: 'h',
        description: 'Print this usage guide',
    },
];
const usageDocs = [
    {
        header: 'PG&E Scraper',
        content: 'Logs on to PG&E\'s customer portal using PhantomJS and retrieves utility usage data.'
    },
    {
        header: 'Options',
        optionList: optionDefinitions,
    },
];
const options = commandLineArgs(optionDefinitions);
const usage = commandLineUsage(usageDocs);

if ('help' in options) {
    console.log(usage);
    return process.exit(0);
}

let USERNAME, PASSWORD;

if (options.username && options.password) {
    USERNAME = options.username;
    PASSWORD = options.password;
} else if (options['creds-file']) {
    let credsFile = require(options['creds-file']);
    USERNAME = credsFile.username || credsFile.USERNAME || credsFile.user || credsFile.USER;
    PASSWORD = credsFile.password || credsFile.PASSWORD || credsFile.pass || credsFile.PASS;
}

if (!USERNAME || !PASSWORD) {
    console.error('Error: no options for credentials passed!');
    return process.exit(1);
}

return loadPhantomAndGetData(USERNAME, PASSWORD)
.then((data) => {
    console.log(`>>> Collected ${data.electricity.reads.length} total reads.`);
    if (options['to-file']) {
        fs.writeFileSync(options['to-file'], JSON.stringify(data, null, 2));
        console.log(`>>> Data written to ${path.join(__dirname, options['to-file'])}.`);
    }
    console.log(">>> Done. Exiting...");
    return process.exit(0);
}).catch((error) => console.error(error));
