const Promise = require('bluebird'); // jshint ignore:line
const promiseWhile = require('while-promise')(Promise);

const qTimeout = require('./qTimeout');

/**
 * Wait for the array of element selectors to return at least one element on
 * page, then resolve the promise.
 *
 * @param {Page} page
 * @param {Array<String>} elementSelectors
 * @return {Promise} 
 */
module.exports = function waitFor(page, ...elementSelectors) {
    let numberOfElements = 0;
    return promiseWhile(() => {
        return numberOfElements < elementSelectors.length;
    }, () => {
        return qTimeout(100)
        .then(() => {
            return page.evaluate(function(elementSelectors) {
                return window.$ ? $(elementSelectors.join(', ')).length : 0;
            }, elementSelectors);
        }).then((_numberOfElements) => {
            numberOfElements = _numberOfElements;
        });
    });
};
