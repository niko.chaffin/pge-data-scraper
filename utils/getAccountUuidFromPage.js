const Promise = require('bluebird'); // jshint ignore:line
const promiseWhile = require('while-promise')(Promise);

const qTimeout = require('./qTimeout');

/**
 * Get the accountUuid query param from the page's url (usually the only query
 * param). Resolves once any string is available to returned.
 *
 * @param {Page} page
 * @return {Promise<String>}
 */
module.exports = function getAccountUuidFromPage(page) {
    let accountUuid = null;
    return promiseWhile(() => {
        return accountUuid === null || accountUuid === '00000000-0000-0000-0000-000000000000';
    }, () => {
        return qTimeout(200).then(() => {
            return page.evaluate(function() {
                return window.location.hash.split('?').pop().split('=').pop();
            }).then((_accountUuid) => {
                accountUuid = _accountUuid;
            });
        });
    }).then(() => accountUuid);
};
