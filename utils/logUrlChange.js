let lastUrl = '';
module.exports = function logUrlChange(page) {
    return page.property('url').then((url) => {
        if (url !== lastUrl) {
            lastUrl = url;
            console.log("> URL changed: " + url);
        }
    });
};
