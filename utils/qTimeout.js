/**
 * Wait a nuber of seconds, then resolve the promise
 *
 * @param {Number} time number of milliseconds to wait
 * @return {Promise}
 */
module.exports = function qTimeout(time) {
    return new Promise(function(resolve) {
        setTimeout(resolve, time);
    });
};
