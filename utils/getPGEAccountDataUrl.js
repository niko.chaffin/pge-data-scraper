/**
 * Helper function for building the URL for requesting utility data (either
 * electricity or gas, based on the accountUuid).
 * Meant to be exeuted within the PhantomJS page context.
 *
 * @param {String} accountUuid
 * @param {Object} params
 * @param {String} params.startDate Date in "YYYY-MM-DD" format
 * @param {String} params.endDate Date in "YYYY-MM-DD" format
 * @param {String} [params.aggregateType=day] one of three values ["bill", "day", "hour"]
 * @return {String}
 */
module.exports = function getPGEAccountDataUrl(accountUuid, params) {
    params = params || {};
    var urlRoot = "/ei/edge/apis/DataBrowser-v1/cost/utilityAccount/";
    var queryParams = {
        startDate: params.startDate + "T00%3A00-0800",
        endDate: params.endDate + "T00%3A00-0800",
        aggregateType: params.aggregateType || "day",
    };
    var paramsList = [];
    for (var param in queryParams) {
        paramsList.push(param + "=" + queryParams[param]);
    }
    return urlRoot + accountUuid + "?" + paramsList.join('&');
};
